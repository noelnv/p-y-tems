# ./yitems.py
# -*- coding: utf-8 -*-
# PyXB bindings for NM:e92452c8d3e28a9e27abfc9994d2007779e7f4c9
# Generated 2015-08-14 10:44:14.325400 by PyXB version 1.2.4 using Python 2.7.6.final.0
# Namespace AbsentNamespace0

from __future__ import unicode_literals
import pyxb
import pyxb.binding
import pyxb.binding.saxer
import io
import pyxb.utils.utility
import pyxb.utils.domutils
import sys
import pyxb.utils.six as _six

# Unique identifier for bindings created at the same time
_GenerationUID = pyxb.utils.utility.UniqueIdentifier('urn:uuid:506f58dc-429b-11e5-acda-20c9d0478c47')

# Version of PyXB used to generate the bindings
_PyXBVersion = '1.2.4'
# Generated bindings are not compatible across PyXB versions
if pyxb.__version__ != _PyXBVersion:
    raise pyxb.PyXBVersionError(_PyXBVersion)

# Import bindings for namespaces imported into schema
import pyxb.binding.datatypes

# NOTE: All namespace declarations are reserved within the binding
Namespace = pyxb.namespace.CreateAbsentNamespace()
Namespace.configureCategories(['typeBinding', 'elementBinding'])

def CreateFromDocument (xml_text, default_namespace=None, location_base=None):
    """Parse the given XML and use the document element to create a
    Python instance.

    @param xml_text An XML document.  This should be data (Python 2
    str or Python 3 bytes), or a text (Python 2 unicode or Python 3
    str) in the L{pyxb._InputEncoding} encoding.

    @keyword default_namespace The L{pyxb.Namespace} instance to use as the
    default namespace where there is no default namespace in scope.
    If unspecified or C{None}, the namespace of the module containing
    this function will be used.

    @keyword location_base: An object to be recorded as the base of all
    L{pyxb.utils.utility.Location} instances associated with events and
    objects handled by the parser.  You might pass the URI from which
    the document was obtained.
    """

    if pyxb.XMLStyle_saxer != pyxb._XMLStyle:
        dom = pyxb.utils.domutils.StringToDOM(xml_text)
        return CreateFromDOM(dom.documentElement, default_namespace=default_namespace)
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    saxer = pyxb.binding.saxer.make_parser(fallback_namespace=default_namespace, location_base=location_base)
    handler = saxer.getContentHandler()
    xmld = xml_text
    if isinstance(xmld, _six.text_type):
        xmld = xmld.encode(pyxb._InputEncoding)
    saxer.parse(io.BytesIO(xmld))
    instance = handler.rootObject()
    return instance

def CreateFromDOM (node, default_namespace=None):
    """Create a Python instance from the given DOM node.
    The node tag must correspond to an element declaration in this module.

    @deprecated: Forcing use of DOM interface is unnecessary; use L{CreateFromDocument}."""
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    return pyxb.binding.basis.element.AnyCreateFromDOM(node, default_namespace)


# Atomic simple type: positiveshort
class positiveshort (pyxb.binding.datatypes.short):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'positiveshort')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 17, 1)
    _Documentation = None
positiveshort._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=positiveshort, value=pyxb.binding.datatypes.short(32767))
positiveshort._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=positiveshort, value=pyxb.binding.datatypes.short(0))
positiveshort._InitializeFacetMap(positiveshort._CF_maxInclusive,
   positiveshort._CF_minInclusive)
Namespace.addCategoryObject('typeBinding', 'positiveshort', positiveshort)

# Atomic simple type: [anonymous]
class STD_ANON (pyxb.binding.datatypes.NMTOKEN, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 93, 3)
    _Documentation = None
STD_ANON._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON, enum_prefix=None)
STD_ANON.set_ = STD_ANON._CF_enumeration.addEnumeration(unicode_value='set', tag='set_')
STD_ANON.list = STD_ANON._CF_enumeration.addEnumeration(unicode_value='list', tag='list')
STD_ANON.collection = STD_ANON._CF_enumeration.addEnumeration(unicode_value='collection', tag='collection')
STD_ANON._InitializeFacetMap(STD_ANON._CF_enumeration)

# Atomic simple type: [anonymous]
class STD_ANON_ (pyxb.binding.datatypes.NMTOKEN, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 264, 3)
    _Documentation = None
STD_ANON_._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_, enum_prefix=None)
STD_ANON_.one = STD_ANON_._CF_enumeration.addEnumeration(unicode_value='one', tag='one')
STD_ANON_.many = STD_ANON_._CF_enumeration.addEnumeration(unicode_value='many', tag='many')
STD_ANON_._InitializeFacetMap(STD_ANON_._CF_enumeration)

# Atomic simple type: [anonymous]
class STD_ANON_2 (pyxb.binding.datatypes.NMTOKEN, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 288, 3)
    _Documentation = None
STD_ANON_2._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_2, enum_prefix=None)
STD_ANON_2.set_ = STD_ANON_2._CF_enumeration.addEnumeration(unicode_value='set', tag='set_')
STD_ANON_2.list = STD_ANON_2._CF_enumeration.addEnumeration(unicode_value='list', tag='list')
STD_ANON_2.collection = STD_ANON_2._CF_enumeration.addEnumeration(unicode_value='collection', tag='collection')
STD_ANON_2._InitializeFacetMap(STD_ANON_2._CF_enumeration)

# Atomic simple type: [anonymous]
class STD_ANON_3 (pyxb.binding.datatypes.NMTOKEN, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 921, 3)
    _Documentation = None
STD_ANON_3._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_3, enum_prefix=None)
STD_ANON_3.property_ = STD_ANON_3._CF_enumeration.addEnumeration(unicode_value='property', tag='property_')
STD_ANON_3.jalo = STD_ANON_3._CF_enumeration.addEnumeration(unicode_value='jalo', tag='jalo')
STD_ANON_3.cmp = STD_ANON_3._CF_enumeration.addEnumeration(unicode_value='cmp', tag='cmp')
STD_ANON_3.dynamic = STD_ANON_3._CF_enumeration.addEnumeration(unicode_value='dynamic', tag='dynamic')
STD_ANON_3._InitializeFacetMap(STD_ANON_3._CF_enumeration)

# Atomic simple type: [anonymous]
class STD_ANON_4 (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 967, 4)
    _Documentation = None
STD_ANON_4._InitializeFacetMap()

# Atomic simple type: defaultValueType
class defaultValueType (pyxb.binding.datatypes.string):

    """Defines a default value text."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'defaultValueType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 979, 1)
    _Documentation = 'Defines a default value text.'
defaultValueType._InitializeFacetMap()
Namespace.addCategoryObject('typeBinding', 'defaultValueType', defaultValueType)

# Atomic simple type: enumCodeType
class enumCodeType (pyxb.binding.datatypes.normalizedString):

    """Configures the code of an enumeration value element. Must start with a letter or underscore."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'enumCodeType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 1015, 1)
    _Documentation = 'Configures the code of an enumeration value element. Must start with a letter or underscore.'
enumCodeType._CF_pattern = pyxb.binding.facets.CF_pattern()
enumCodeType._CF_pattern.addPattern(pattern='([a-zA-Z_])+([a-z_A-Z$0-9])*')
enumCodeType._InitializeFacetMap(enumCodeType._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'enumCodeType', enumCodeType)

# Atomic simple type: codeType
class codeType (pyxb.binding.datatypes.normalizedString):

    """Configures the code of an element."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'codeType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 1024, 1)
    _Documentation = 'Configures the code of an element.'
codeType._InitializeFacetMap()
Namespace.addCategoryObject('typeBinding', 'codeType', codeType)

# Atomic simple type: deploymentRefType
class deploymentRefType (pyxb.binding.datatypes.normalizedString):

    """Deprecated. Defines a reference to a deployment definition."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'deploymentRefType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 1031, 1)
    _Documentation = 'Deprecated. Defines a reference to a deployment definition.'
deploymentRefType._InitializeFacetMap()
Namespace.addCategoryObject('typeBinding', 'deploymentRefType', deploymentRefType)

# Atomic simple type: classType
class classType (pyxb.binding.datatypes.normalizedString):

    """Configures the class to use for enclosing element."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'classType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 1038, 1)
    _Documentation = 'Configures the class to use for enclosing element.'
classType._InitializeFacetMap()
Namespace.addCategoryObject('typeBinding', 'classType', classType)

# Complex type atomictypesType with content type ELEMENT_ONLY
class atomictypesType (pyxb.binding.basis.complexTypeDefinition):
    """
				Defines a list of atomic types.
			"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'atomictypesType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 50, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element atomictype uses Python identifier atomictype
    __atomictype = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'atomictype'), 'atomictype', '__AbsentNamespace0_atomictypesType_atomictype', True, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 57, 3), )

    
    atomictype = property(__atomictype.value, __atomictype.set, None, "An AtomicType represents a simple java object. (The name 'atomic' just means 'non-composed' objects.)")

    _ElementMap.update({
        __atomictype.name() : __atomictype
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'atomictypesType', atomictypesType)


# Complex type collectiontypesType with content type ELEMENT_ONLY
class collectiontypesType (pyxb.binding.basis.complexTypeDefinition):
    """
				Defines a list of collection types.
			"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'collectiontypesType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 114, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element collectiontype uses Python identifier collectiontype
    __collectiontype = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'collectiontype'), 'collectiontype', '__AbsentNamespace0_collectiontypesType_collectiontype', True, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 121, 3), )

    
    collectiontype = property(__collectiontype.value, __collectiontype.set, None, 'A CollectionType defines a collection of typed elements.')

    _ElementMap.update({
        __collectiontype.name() : __collectiontype
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'collectiontypesType', collectiontypesType)


# Complex type relationsType with content type ELEMENT_ONLY
class relationsType (pyxb.binding.basis.complexTypeDefinition):
    """
				Defines a list of relation types.
			"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'relationsType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 204, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element relation uses Python identifier relation
    __relation = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'relation'), 'relation', '__AbsentNamespace0_relationsType_relation', True, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 211, 3), )

    
    relation = property(__relation.value, __relation.set, None, 'A RelationType defines a n-m or 1-n relation between types.')

    _ElementMap.update({
        __relation.name() : __relation
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'relationsType', relationsType)


# Complex type enumtypesType with content type ELEMENT_ONLY
class enumtypesType (pyxb.binding.basis.complexTypeDefinition):
    """
				Defines a list of enumeration types.
			"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'enumtypesType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 357, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element enumtype uses Python identifier enumtype
    __enumtype = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'enumtype'), 'enumtype', '__AbsentNamespace0_enumtypesType_enumtype', True, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 364, 3), )

    
    enumtype = property(__enumtype.value, __enumtype.set, None, 'An EnumerationType defines fixed value types. (The typesystem provides item enumeration only)')

    _ElementMap.update({
        __enumtype.name() : __enumtype
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'enumtypesType', enumtypesType)


# Complex type indexType with content type ELEMENT_ONLY
class indexType (pyxb.binding.basis.complexTypeDefinition):
    """Configures a database index for enclosing type."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'indexType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 372, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element key uses Python identifier key
    __key = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'key'), 'key', '__AbsentNamespace0_indexType_key', True, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 377, 3), )

    
    key = property(__key.value, __key.set, None, 'Configures a single index key.')

    
    # Attribute name uses Python identifier name
    __name = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'name'), 'name', '__AbsentNamespace0_indexType_name', pyxb.binding.datatypes.string, required=True)
    __name._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 383, 2)
    __name._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 383, 2)
    
    name = property(__name.value, __name.set, None, 'The name prefix of the index.')

    
    # Attribute remove uses Python identifier remove
    __remove = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'remove'), 'remove', '__AbsentNamespace0_indexType_remove', pyxb.binding.datatypes.boolean, unicode_default='false')
    __remove._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 388, 2)
    __remove._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 388, 2)
    
    remove = property(__remove.value, __remove.set, None, "If 'true' this index will be ommitted while in initialization process even if there were precendent declarations.This attribute has effect only if replace = true.")

    
    # Attribute replace uses Python identifier replace
    __replace = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'replace'), 'replace', '__AbsentNamespace0_indexType_replace', pyxb.binding.datatypes.boolean, unicode_default='false')
    __replace._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 393, 2)
    __replace._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 393, 2)
    
    replace = property(__replace.value, __replace.set, None, "If 'true' this index is a replacement/redeclaration for already existing index.")

    
    # Attribute unique uses Python identifier unique
    __unique = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unique'), 'unique', '__AbsentNamespace0_indexType_unique', pyxb.binding.datatypes.boolean)
    __unique._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 398, 2)
    __unique._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 398, 2)
    
    unique = property(__unique.value, __unique.set, None, "If 'true', the value of this attribute has to be unique within all instances of this index. Attributes with persistence type set to 'jalo' can not be unique. Default is 'false'.")

    _ElementMap.update({
        __key.name() : __key
    })
    _AttributeMap.update({
        __name.name() : __name,
        __remove.name() : __remove,
        __replace.name() : __replace,
        __unique.name() : __unique
    })
Namespace.addCategoryObject('typeBinding', 'indexType', indexType)


# Complex type indexKeyType with content type EMPTY
class indexKeyType (pyxb.binding.basis.complexTypeDefinition):
    """Configures a single index key."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'indexKeyType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 405, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute attribute uses Python identifier attribute
    __attribute = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'attribute'), 'attribute', '__AbsentNamespace0_indexKeyType_attribute', pyxb.binding.datatypes.string, required=True)
    __attribute._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 409, 2)
    __attribute._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 409, 2)
    
    attribute = property(__attribute.value, __attribute.set, None, 'Type attribute to be indexed.')

    
    # Attribute lower uses Python identifier lower
    __lower = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'lower'), 'lower', '__AbsentNamespace0_indexKeyType_lower', pyxb.binding.datatypes.boolean)
    __lower._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 414, 2)
    __lower._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 414, 2)
    
    lower = property(__lower.value, __lower.set, None, "Elements will be indexed case-insensitive. Default is 'false'.")

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __attribute.name() : __attribute,
        __lower.name() : __lower
    })
Namespace.addCategoryObject('typeBinding', 'indexKeyType', indexKeyType)


# Complex type attributeType with content type ELEMENT_ONLY
class attributeType (pyxb.binding.basis.complexTypeDefinition):
    """Defines an attribute of a type."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'attributeType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 421, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element defaultvalue uses Python identifier defaultvalue
    __defaultvalue = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'defaultvalue'), 'defaultvalue', '__AbsentNamespace0_attributeType_defaultvalue', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 426, 3), )

    
    defaultvalue = property(__defaultvalue.value, __defaultvalue.set, None, 'Configures a default value for this attribute used if no value is provided. The default value is calculated by initialization and will not be re-calculated by runtime.')

    
    # Element description uses Python identifier description
    __description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'description'), 'description', '__AbsentNamespace0_attributeType_description', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 431, 3), )

    
    description = property(__description.value, __description.set, None, 'Gives a description for this attribute only used for the javadoc of generated attribute methods.')

    
    # Element persistence uses Python identifier persistence
    __persistence = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'persistence'), 'persistence', '__AbsentNamespace0_attributeType_persistence', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 436, 3), )

    
    persistence = property(__persistence.value, __persistence.set, None, "Defines how the values of the attribute will be stored. Possible values: 'cmp' (deprecated), 'jalo' (not persistent, deprecated), 'property' (persistent), 'dynamic' (not persisted).")

    
    # Element modifiers uses Python identifier modifiers
    __modifiers = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'modifiers'), 'modifiers', '__AbsentNamespace0_attributeType_modifiers', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 441, 3), )

    
    modifiers = property(__modifiers.value, __modifiers.set, None, 'Configures advanced settings for this attribute definition.')

    
    # Element custom-properties uses Python identifier custom_properties
    __custom_properties = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'custom-properties'), 'custom_properties', '__AbsentNamespace0_attributeType_custom_properties', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 446, 3), )

    
    custom_properties = property(__custom_properties.value, __custom_properties.set, None, 'Allows to configure custom properties for this attribute.')

    
    # Element model uses Python identifier model
    __model = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'model'), 'model', '__AbsentNamespace0_attributeType_model', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 451, 3), )

    
    model = property(__model.value, __model.set, None, 'Allows to configure model generation settings for this attribute. Models are used by the hybris ServiceLayer.')

    
    # Attribute redeclare uses Python identifier redeclare
    __redeclare = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'redeclare'), 'redeclare', '__AbsentNamespace0_attributeType_redeclare', pyxb.binding.datatypes.boolean)
    __redeclare._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 457, 2)
    __redeclare._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 457, 2)
    
    redeclare = property(__redeclare.value, __redeclare.set, None, "Lets you re-define the attribute definition from an inherited type. In essence, you can use a different type of attribute as well as different modifier combinations than on the supertype. Default is 'false'.")

    
    # Attribute qualifier uses Python identifier qualifier
    __qualifier = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'qualifier'), 'qualifier', '__AbsentNamespace0_attributeType_qualifier', pyxb.binding.datatypes.string, required=True)
    __qualifier._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 462, 2)
    __qualifier._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 462, 2)
    
    qualifier = property(__qualifier.value, __qualifier.set, None, 'Qualifier of this attribute. Attribute qualifiers\tmust be unique across a single type.')

    
    # Attribute type uses Python identifier type
    __type = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'type'), 'type', '__AbsentNamespace0_attributeType_type', pyxb.binding.datatypes.string, required=True)
    __type._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 467, 2)
    __type._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 467, 2)
    
    type = property(__type.value, __type.set, None, "The type of the attribute, such as 'Product', 'int' or 'java.lang.String'. Primitive java types will be mapped to the corresponding atomic type. For example: 'int' will be mapped to the atomic type\t'java.lang.Integer' with implicit default value.")

    
    # Attribute metatype uses Python identifier metatype
    __metatype = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'metatype'), 'metatype', '__AbsentNamespace0_attributeType_metatype', pyxb.binding.datatypes.string)
    __metatype._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 472, 2)
    __metatype._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 472, 2)
    
    metatype = property(__metatype.value, __metatype.set, None, "Advanced setting. Specifies the metatype for the attributes definition. Must be a type extending AttributeDescriptor. Default is 'AttributeDescriptor'.")

    
    # Attribute autocreate uses Python identifier autocreate
    __autocreate = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'autocreate'), 'autocreate', '__AbsentNamespace0_attributeType_autocreate', pyxb.binding.datatypes.boolean, unicode_default='true')
    __autocreate._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 477, 2)
    __autocreate._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 477, 2)
    
    autocreate = property(__autocreate.value, __autocreate.set, None, "If 'true', the attribute descriptor will be created during initialization. Default is 'true'.")

    
    # Attribute generate uses Python identifier generate
    __generate = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'generate'), 'generate', '__AbsentNamespace0_attributeType_generate', pyxb.binding.datatypes.boolean, unicode_default='true')
    __generate._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 482, 2)
    __generate._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 482, 2)
    
    generate = property(__generate.value, __generate.set, None, "If 'true', getter and setter methods for this\tattribute will be generated during a hybris Suite build. Default is 'true'.")

    
    # Attribute isSelectionOf uses Python identifier isSelectionOf
    __isSelectionOf = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'isSelectionOf'), 'isSelectionOf', '__AbsentNamespace0_attributeType_isSelectionOf', pyxb.binding.datatypes.string)
    __isSelectionOf._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 487, 2)
    __isSelectionOf._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 487, 2)
    
    isSelectionOf = property(__isSelectionOf.value, __isSelectionOf.set, None, "References an attribute of the same type. Only values of the referenced attribute can be selected\tas values for this attribute. Typical example: the default delivery address of a customer must be one of the addresses set for the customer. Default is 'false'.")

    _ElementMap.update({
        __defaultvalue.name() : __defaultvalue,
        __description.name() : __description,
        __persistence.name() : __persistence,
        __modifiers.name() : __modifiers,
        __custom_properties.name() : __custom_properties,
        __model.name() : __model
    })
    _AttributeMap.update({
        __redeclare.name() : __redeclare,
        __qualifier.name() : __qualifier,
        __type.name() : __type,
        __metatype.name() : __metatype,
        __autocreate.name() : __autocreate,
        __generate.name() : __generate,
        __isSelectionOf.name() : __isSelectionOf
    })
Namespace.addCategoryObject('typeBinding', 'attributeType', attributeType)


# Complex type attributeModelType with content type ELEMENT_ONLY
class attributeModelType (pyxb.binding.basis.complexTypeDefinition):
    """Allows to configure model generation for this attribute used at servicelayer."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'attributeModelType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 494, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element getter uses Python identifier getter
    __getter = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'getter'), 'getter', '__AbsentNamespace0_attributeModelType_getter', True, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 499, 3), )

    
    getter = property(__getter.value, __getter.set, None, '\n\t\t\t\t\t\tAllows to configure alternative getter methods at generated model.\n\t\t\t\t\t')

    
    # Element setter uses Python identifier setter
    __setter = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'setter'), 'setter', '__AbsentNamespace0_attributeModelType_setter', True, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 506, 3), )

    
    setter = property(__setter.value, __setter.set, None, '\n\t\t\t\t\t\tAllows to configure alternative setter methods at generated model.\n\t\t\t\t\t')

    
    # Attribute generate uses Python identifier generate
    __generate = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'generate'), 'generate', '__AbsentNamespace0_attributeModelType_generate', pyxb.binding.datatypes.boolean, unicode_default='true')
    __generate._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 514, 2)
    __generate._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 514, 2)
    
    generate = property(__generate.value, __generate.set, None, "Whether getter and setter methods for the model representation of the attribute will be generated. Default is 'true'.")

    _ElementMap.update({
        __getter.name() : __getter,
        __setter.name() : __setter
    })
    _AttributeMap.update({
        __generate.name() : __generate
    })
Namespace.addCategoryObject('typeBinding', 'attributeModelType', attributeModelType)


# Complex type itemModelType with content type ELEMENT_ONLY
class itemModelType (pyxb.binding.basis.complexTypeDefinition):
    """Allows to configure model generation for this item used at servicelayer."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'itemModelType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 521, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element constructor uses Python identifier constructor
    __constructor = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'constructor'), 'constructor', '__AbsentNamespace0_itemModelType_constructor', True, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 526, 3), )

    
    constructor = property(__constructor.value, __constructor.set, None, '\n\t\t\t\t\t\tAllows to configure model constructor signatures.\n\t\t\t\t\t')

    
    # Attribute generate uses Python identifier generate
    __generate = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'generate'), 'generate', '__AbsentNamespace0_itemModelType_generate', pyxb.binding.datatypes.boolean, unicode_default='true')
    __generate._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 534, 2)
    __generate._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 534, 2)
    
    generate = property(__generate.value, __generate.set, None, "Whether a model for the type and models for subtypes will be generated. Default is 'true'.")

    _ElementMap.update({
        __constructor.name() : __constructor
    })
    _AttributeMap.update({
        __generate.name() : __generate
    })
Namespace.addCategoryObject('typeBinding', 'itemModelType', itemModelType)


# Complex type modelConstructorType with content type EMPTY
class modelConstructorType (pyxb.binding.basis.complexTypeDefinition):
    """Allows to configure model constructor signatures."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'modelConstructorType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 541, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute signature uses Python identifier signature
    __signature = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'signature'), 'signature', '__AbsentNamespace0_modelConstructorType_signature', pyxb.binding.datatypes.string, required=True)
    __signature._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 545, 2)
    __signature._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 545, 2)
    
    signature = property(__signature.value, __signature.set, None, 'Add here, as comma separated list, the attribute qualifiers for the constructor signature in the model.')

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __signature.name() : __signature
    })
Namespace.addCategoryObject('typeBinding', 'modelConstructorType', modelConstructorType)


# Complex type modelMethodType with content type EMPTY
class modelMethodType (pyxb.binding.basis.complexTypeDefinition):
    """Allows to configure alternative methods at generated model."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'modelMethodType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 552, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute name uses Python identifier name
    __name = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'name'), 'name', '__AbsentNamespace0_modelMethodType_name', pyxb.binding.datatypes.string, required=True)
    __name._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 556, 2)
    __name._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 556, 2)
    
    name = property(__name.value, __name.set, None, '\n\t\t\t\t\tName of the alternative getter method.\n\t\t\t\t')

    
    # Attribute deprecated uses Python identifier deprecated
    __deprecated = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'deprecated'), 'deprecated', '__AbsentNamespace0_modelMethodType_deprecated', pyxb.binding.datatypes.boolean, unicode_default='false')
    __deprecated._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 563, 2)
    __deprecated._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 563, 2)
    
    deprecated = property(__deprecated.value, __deprecated.set, None, '\n\t\t\t\t\tWill the method be marked deprecated? Default is\n\t\t\t\t\tfalse.\n\t\t\t\t')

    
    # Attribute default uses Python identifier default
    __default = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'default'), 'default', '__AbsentNamespace0_modelMethodType_default', pyxb.binding.datatypes.boolean, unicode_default='false')
    __default._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 571, 2)
    __default._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 571, 2)
    
    default = property(__default.value, __default.set, None, 'Will this method be the default method and replace the original one instead of adding it additional? Default is false.')

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __name.name() : __name,
        __deprecated.name() : __deprecated,
        __default.name() : __default
    })
Namespace.addCategoryObject('typeBinding', 'modelMethodType', modelMethodType)


# Complex type customPropertiesType with content type ELEMENT_ONLY
class customPropertiesType (pyxb.binding.basis.complexTypeDefinition):
    """Defines custom properties."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'customPropertiesType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 578, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element property uses Python identifier property_
    __property = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'property'), 'property_', '__AbsentNamespace0_customPropertiesType_property', True, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 583, 3), )

    
    property_ = property(__property.value, __property.set, None, 'Defines a custom property.')

    _ElementMap.update({
        __property.name() : __property
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'customPropertiesType', customPropertiesType)


# Complex type customPropertyType with content type ELEMENT_ONLY
class customPropertyType (pyxb.binding.basis.complexTypeDefinition):
    """Defines a custom property."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'customPropertyType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 591, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element value uses Python identifier value_
    __value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'value'), 'value_', '__AbsentNamespace0_customPropertyType_value', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 596, 3), )

    
    value_ = property(__value.value, __value.set, None, 'The value of the custom property.')

    
    # Attribute name uses Python identifier name
    __name = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'name'), 'name', '__AbsentNamespace0_customPropertyType_name', pyxb.binding.datatypes.string, required=True)
    __name._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 602, 2)
    __name._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 602, 2)
    
    name = property(__name.value, __name.set, None, 'The name of the custom property.')

    _ElementMap.update({
        __value.name() : __value
    })
    _AttributeMap.update({
        __name.name() : __name
    })
Namespace.addCategoryObject('typeBinding', 'customPropertyType', customPropertyType)


# Complex type attributesType with content type ELEMENT_ONLY
class attributesType (pyxb.binding.basis.complexTypeDefinition):
    """Configures a list of attributes."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'attributesType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 609, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element attribute uses Python identifier attribute
    __attribute = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'attribute'), 'attribute', '__AbsentNamespace0_attributesType_attribute', True, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 614, 3), )

    
    attribute = property(__attribute.value, __attribute.set, None, 'Defines a single attribute.')

    _ElementMap.update({
        __attribute.name() : __attribute
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'attributesType', attributesType)


# Complex type indexesType with content type ELEMENT_ONLY
class indexesType (pyxb.binding.basis.complexTypeDefinition):
    """Configures a list of indexes."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'indexesType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 622, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element index uses Python identifier index
    __index = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'index'), 'index', '__AbsentNamespace0_indexesType_index', True, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 627, 3), )

    
    index = property(__index.value, __index.set, None, 'Configures a single index.')

    _ElementMap.update({
        __index.name() : __index
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'indexesType', indexesType)


# Complex type itemtypesType with content type ELEMENT_ONLY
class itemtypesType (pyxb.binding.basis.complexTypeDefinition):
    """Defines a grouping of item types."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'itemtypesType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 723, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element itemtype uses Python identifier itemtype
    __itemtype = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'itemtype'), 'itemtype', '__AbsentNamespace0_itemtypesType_itemtype', True, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 728, 3), )

    
    itemtype = property(__itemtype.value, __itemtype.set, None, 'Specifies a specific ComposedType.')

    
    # Element typegroup uses Python identifier typegroup
    __typegroup = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'typegroup'), 'typegroup', '__AbsentNamespace0_itemtypesType_typegroup', True, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 733, 3), )

    
    typegroup = property(__typegroup.value, __typegroup.set, None, 'Specifies a group of ComposedTypes to allow better structuring within the items.xml file.')

    _ElementMap.update({
        __itemtype.name() : __itemtype,
        __typegroup.name() : __typegroup
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'itemtypesType', itemtypesType)


# Complex type typeGroupType with content type ELEMENT_ONLY
class typeGroupType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type typeGroupType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'typeGroupType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 741, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element itemtype uses Python identifier itemtype
    __itemtype = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'itemtype'), 'itemtype', '__AbsentNamespace0_typeGroupType_itemtype', True, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 743, 3), )

    
    itemtype = property(__itemtype.value, __itemtype.set, None, 'Specifies a specific ComposedType.')

    
    # Attribute name uses Python identifier name
    __name = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'name'), 'name', '__AbsentNamespace0_typeGroupType_name', pyxb.binding.datatypes.string)
    __name._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 749, 2)
    __name._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 749, 2)
    
    name = property(__name.value, __name.set, None, 'Defines the name of this group. Only for structural purpose, will have no effect on runtime. Default is empty.')

    _ElementMap.update({
        __itemtype.name() : __itemtype
    })
    _AttributeMap.update({
        __name.name() : __name
    })
Namespace.addCategoryObject('typeBinding', 'typeGroupType', typeGroupType)


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON (pyxb.binding.basis.complexTypeDefinition):
    """Defines the types of your extension."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 760, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element atomictypes uses Python identifier atomictypes
    __atomictypes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'atomictypes'), 'atomictypes', '__AbsentNamespace0_CTD_ANON_atomictypes', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 762, 4), )

    
    atomictypes = property(__atomictypes.value, __atomictypes.set, None, "Defines the list of AtomicType's for your extension.")

    
    # Element collectiontypes uses Python identifier collectiontypes
    __collectiontypes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'collectiontypes'), 'collectiontypes', '__AbsentNamespace0_CTD_ANON_collectiontypes', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 767, 4), )

    
    collectiontypes = property(__collectiontypes.value, __collectiontypes.set, None, "Defines the list of CollectionType's for your extension.")

    
    # Element enumtypes uses Python identifier enumtypes
    __enumtypes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'enumtypes'), 'enumtypes', '__AbsentNamespace0_CTD_ANON_enumtypes', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 772, 4), )

    
    enumtypes = property(__enumtypes.value, __enumtypes.set, None, "Defines the list of EnumerationType's for your extension.")

    
    # Element maptypes uses Python identifier maptypes
    __maptypes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'maptypes'), 'maptypes', '__AbsentNamespace0_CTD_ANON_maptypes', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 777, 4), )

    
    maptypes = property(__maptypes.value, __maptypes.set, None, "Defines the list of MapType's for your extension.")

    
    # Element relations uses Python identifier relations
    __relations = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'relations'), 'relations', '__AbsentNamespace0_CTD_ANON_relations', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 782, 4), )

    
    relations = property(__relations.value, __relations.set, None, "Defines the list of RelationType's for your extension.")

    
    # Element itemtypes uses Python identifier itemtypes
    __itemtypes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'itemtypes'), 'itemtypes', '__AbsentNamespace0_CTD_ANON_itemtypes', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 787, 4), )

    
    itemtypes = property(__itemtypes.value, __itemtypes.set, None, "Defines the list of ComposedType's for your extension.")

    _ElementMap.update({
        __atomictypes.name() : __atomictypes,
        __collectiontypes.name() : __collectiontypes,
        __enumtypes.name() : __enumtypes,
        __maptypes.name() : __maptypes,
        __relations.name() : __relations,
        __itemtypes.name() : __itemtypes
    })
    _AttributeMap.update({
        
    })



# Complex type maptypesType with content type ELEMENT_ONLY
class maptypesType (pyxb.binding.basis.complexTypeDefinition):
    """Specifies a list of map types."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'maptypesType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 832, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element maptype uses Python identifier maptype
    __maptype = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'maptype'), 'maptype', '__AbsentNamespace0_maptypesType_maptype', True, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 837, 3), )

    
    maptype = property(__maptype.value, __maptype.set, None, 'Like the java collection framework, a type, which defines map objects. Attention: When used as type for an attribute, the attribute will not be searchable and the access performance is not effective. Consider to use a relation.')

    _ElementMap.update({
        __maptype.name() : __maptype
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'maptypesType', maptypesType)


# Complex type modifiersType with content type EMPTY
class modifiersType (pyxb.binding.basis.complexTypeDefinition):
    """Specifies further properties of an attribute which can be redeclared at other extensions."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'modifiersType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 845, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute read uses Python identifier read
    __read = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'read'), 'read', '__AbsentNamespace0_modifiersType_read', pyxb.binding.datatypes.boolean)
    __read._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 849, 2)
    __read._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 849, 2)
    
    read = property(__read.value, __read.set, None, "Defines if this attribute is readable (that is, if a getter method will be generated). Default is 'true'.\tThe visibility of the getter depends on the value of the private attribute.")

    
    # Attribute write uses Python identifier write
    __write = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'write'), 'write', '__AbsentNamespace0_modifiersType_write', pyxb.binding.datatypes.boolean)
    __write._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 854, 2)
    __write._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 854, 2)
    
    write = property(__write.value, __write.set, None, "Defines if this attribute is writable (that is, if a setter method will be generated). Default is 'true'.\tThe visibility of the setter depends on the value of the private attribute.")

    
    # Attribute search uses Python identifier search
    __search = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'search'), 'search', '__AbsentNamespace0_modifiersType_search', pyxb.binding.datatypes.boolean)
    __search._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 859, 2)
    __search._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 859, 2)
    
    search = property(__search.value, __search.set, None, "Defines if this attribute is searchable by a FlexibleSearch. Default is 'true'. Attributes with persistence type set to 'jalo' can not be searchable.")

    
    # Attribute optional uses Python identifier optional
    __optional = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'optional'), 'optional', '__AbsentNamespace0_modifiersType_optional', pyxb.binding.datatypes.boolean)
    __optional._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 864, 2)
    __optional._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 864, 2)
    
    optional = property(__optional.value, __optional.set, None, "Defines if this attribute is mandatory or optional. Default is 'true' for optional. Set to 'false' for mandatory.")

    
    # Attribute private uses Python identifier private
    __private = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'private'), 'private', '__AbsentNamespace0_modifiersType_private', pyxb.binding.datatypes.boolean)
    __private._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 869, 2)
    __private._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 869, 2)
    
    private = property(__private.value, __private.set, None, "Defines the Java visibility of the generated getter and setter methods for this attribute. If 'true', the visibility modifier of generated methods is set to 'protected'; if 'false', the visibility modifier is 'public'. Default is 'false' for 'public' generated methods. Also, you will have no generated methods in the ServiceLayer if 'true'.")

    
    # Attribute initial uses Python identifier initial
    __initial = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'initial'), 'initial', '__AbsentNamespace0_modifiersType_initial', pyxb.binding.datatypes.boolean)
    __initial._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 874, 2)
    __initial._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 874, 2)
    
    initial = property(__initial.value, __initial.set, None, "If 'true', the attribute will only be writable during the item creation. Setting this to 'true' is only useful in combination with write='false'.\tDefault is 'false'.")

    
    # Attribute removable uses Python identifier removable
    __removable = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'removable'), 'removable', '__AbsentNamespace0_modifiersType_removable', pyxb.binding.datatypes.boolean)
    __removable._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 879, 2)
    __removable._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 879, 2)
    
    removable = property(__removable.value, __removable.set, None, "Defines if this attribute is removable. Default is 'true'.")

    
    # Attribute partof uses Python identifier partof
    __partof = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'partof'), 'partof', '__AbsentNamespace0_modifiersType_partof', pyxb.binding.datatypes.boolean)
    __partof._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 884, 2)
    __partof._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 884, 2)
    
    partof = property(__partof.value, __partof.set, None, "Defines if the assigned attribute value only belongs to the current instance of this type. Default is 'false'.")

    
    # Attribute unique uses Python identifier unique
    __unique = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unique'), 'unique', '__AbsentNamespace0_modifiersType_unique', pyxb.binding.datatypes.boolean)
    __unique._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 889, 2)
    __unique._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 889, 2)
    
    unique = property(__unique.value, __unique.set, None, "If 'true', the value of this attribute has to be unique within all instances of this type. If there are multiple attributes marked as unique, then their combined values must be unique. Will not be evaluated at jalo layer, if you want to manage the attribute directly using jalo layer you have to ensure uniqueness manually. Default is 'false'.")

    
    # Attribute dontOptimize uses Python identifier dontOptimize
    __dontOptimize = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'dontOptimize'), 'dontOptimize', '__AbsentNamespace0_modifiersType_dontOptimize', pyxb.binding.datatypes.boolean)
    __dontOptimize._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 894, 2)
    __dontOptimize._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 894, 2)
    
    dontOptimize = property(__dontOptimize.value, __dontOptimize.set, None, "If 'true' the attribute value will be stored in the 'global' property table, otherwise a separate column (inside the table of the associated type)will be created for storing its values. Default is 'false'.")

    
    # Attribute encrypted uses Python identifier encrypted
    __encrypted = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'encrypted'), 'encrypted', '__AbsentNamespace0_modifiersType_encrypted', pyxb.binding.datatypes.boolean)
    __encrypted._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 899, 2)
    __encrypted._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 899, 2)
    
    encrypted = property(__encrypted.value, __encrypted.set, None, "If 'true', the attribute value will be stored in an encrypted way. Default is 'false'.")

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __read.name() : __read,
        __write.name() : __write,
        __search.name() : __search,
        __optional.name() : __optional,
        __private.name() : __private,
        __initial.name() : __initial,
        __removable.name() : __removable,
        __partof.name() : __partof,
        __unique.name() : __unique,
        __dontOptimize.name() : __dontOptimize,
        __encrypted.name() : __encrypted
    })
Namespace.addCategoryObject('typeBinding', 'modifiersType', modifiersType)


# Complex type columntypeType with content type ELEMENT_ONLY
class columntypeType (pyxb.binding.basis.complexTypeDefinition):
    """Configures a persistence definition for a specific database."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'columntypeType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 958, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element value uses Python identifier value_
    __value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'value'), 'value_', '__AbsentNamespace0_columntypeType_value', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 963, 3), )

    
    value_ = property(__value.value, __value.set, None, "The attribute type used in the create statement of the database table, such as 'varchar2(4000)'.")

    
    # Attribute database uses Python identifier database
    __database = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'database'), 'database', '__AbsentNamespace0_columntypeType_database', pyxb.binding.datatypes.string)
    __database._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 972, 2)
    __database._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 972, 2)
    
    database = property(__database.value, __database.set, None, "The database the given definition will be used for. One of 'oracle', 'mysql', 'sqlserver' or 'hsql'. Default is empty which configures fallback for non specified databases.")

    _ElementMap.update({
        __value.name() : __value
    })
    _AttributeMap.update({
        __database.name() : __database
    })
Namespace.addCategoryObject('typeBinding', 'columntypeType', columntypeType)


# Complex type atomictypeType with content type EMPTY
class atomictypeType (pyxb.binding.basis.complexTypeDefinition):
    """An AtomicType represents a simple java object. (The name 'atomic' just means 'non-composed' objects.)"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'atomictypeType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 24, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute class uses Python identifier class_
    __class = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'class'), 'class_', '__AbsentNamespace0_atomictypeType_class', classType, required=True)
    __class._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 28, 2)
    __class._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 28, 2)
    
    class_ = property(__class.value, __class.set, None, 'Corresponding Java class in the hybris Suite; will also be used as the code of the atomic type.')

    
    # Attribute autocreate uses Python identifier autocreate
    __autocreate = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'autocreate'), 'autocreate', '__AbsentNamespace0_atomictypeType_autocreate', pyxb.binding.datatypes.boolean, unicode_default='true')
    __autocreate._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 33, 2)
    __autocreate._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 33, 2)
    
    autocreate = property(__autocreate.value, __autocreate.set, None, "If 'true', the AtomicType will be created during initialization.")

    
    # Attribute generate uses Python identifier generate
    __generate = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'generate'), 'generate', '__AbsentNamespace0_atomictypeType_generate', pyxb.binding.datatypes.boolean, unicode_default='true')
    __generate._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 38, 2)
    __generate._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 38, 2)
    
    generate = property(__generate.value, __generate.set, None, "Deprecated. Has no effect for atomic types. Default is 'true'.")

    
    # Attribute extends uses Python identifier extends
    __extends = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'extends'), 'extends', '__AbsentNamespace0_atomictypeType_extends', classType)
    __extends._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 43, 2)
    __extends._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 43, 2)
    
    extends = property(__extends.value, __extends.set, None, "Defines the class which will be extended. Default is 'java.lang.Object'.")

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __class.name() : __class,
        __autocreate.name() : __autocreate,
        __generate.name() : __generate,
        __extends.name() : __extends
    })
Namespace.addCategoryObject('typeBinding', 'atomictypeType', atomictypeType)


# Complex type collectiontypeType with content type EMPTY
class collectiontypeType (pyxb.binding.basis.complexTypeDefinition):
    """A CollectionType defines a collection of typed elements. Attention: If using a collection type for persistent attributes (not jalo) you can not search on that attribute and you are limited in size of collection. Consider to use a relation instead."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'collectiontypeType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 65, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute code uses Python identifier code
    __code = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'code'), 'code', '__AbsentNamespace0_collectiontypeType_code', codeType, required=True)
    __code._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 69, 2)
    __code._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 69, 2)
    
    code = property(__code.value, __code.set, None, 'The code (that is, qualifier) of the CollectionType.')

    
    # Attribute elementtype uses Python identifier elementtype
    __elementtype = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'elementtype'), 'elementtype', '__AbsentNamespace0_collectiontypeType_elementtype', codeType, required=True)
    __elementtype._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 74, 2)
    __elementtype._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 74, 2)
    
    elementtype = property(__elementtype.value, __elementtype.set, None, 'The type of elements of this CollectionType.')

    
    # Attribute autocreate uses Python identifier autocreate
    __autocreate = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'autocreate'), 'autocreate', '__AbsentNamespace0_collectiontypeType_autocreate', pyxb.binding.datatypes.boolean, unicode_default='true')
    __autocreate._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 79, 2)
    __autocreate._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 79, 2)
    
    autocreate = property(__autocreate.value, __autocreate.set, None, "If 'true', the CollectionType will be created during initialization.")

    
    # Attribute generate uses Python identifier generate
    __generate = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'generate'), 'generate', '__AbsentNamespace0_collectiontypeType_generate', pyxb.binding.datatypes.boolean, unicode_default='true')
    __generate._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 84, 2)
    __generate._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 84, 2)
    
    generate = property(__generate.value, __generate.set, None, "Deprecated. Has no effect for collection types. Default is 'true'.")

    
    # Attribute type uses Python identifier type
    __type = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'type'), 'type', '__AbsentNamespace0_collectiontypeType_type', STD_ANON, unicode_default='collection')
    __type._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 89, 2)
    __type._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 89, 2)
    
    type = property(__type.value, __type.set, None, "Configures the type of this collection: 'set', 'list', 'collection'. The getter / setter methods will use corresponding Java collection interfaces. Default is 'collection'.")

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __code.name() : __code,
        __elementtype.name() : __elementtype,
        __autocreate.name() : __autocreate,
        __generate.name() : __generate,
        __type.name() : __type
    })
Namespace.addCategoryObject('typeBinding', 'collectiontypeType', collectiontypeType)


# Complex type deploymentType with content type EMPTY
class deploymentType (pyxb.binding.basis.complexTypeDefinition):
    """A deployment defines how a (generic) item or relation is mapped onto the database."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'deploymentType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 129, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute table uses Python identifier table
    __table = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'table'), 'table', '__AbsentNamespace0_deploymentType_table', pyxb.binding.datatypes.string, required=True)
    __table._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 133, 2)
    __table._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 133, 2)
    
    table = property(__table.value, __table.set, None, 'The mapped database table. Must be globally unique.')

    
    # Attribute typecode uses Python identifier typecode
    __typecode = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'typecode'), 'typecode', '__AbsentNamespace0_deploymentType_typecode', positiveshort, required=True)
    __typecode._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 138, 2)
    __typecode._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 138, 2)
    
    typecode = property(__typecode.value, __typecode.set, None, 'The mapped item type code. Must be globally unique')

    
    # Attribute propertytable uses Python identifier propertytable
    __propertytable = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'propertytable'), 'propertytable', '__AbsentNamespace0_deploymentType_propertytable', pyxb.binding.datatypes.string)
    __propertytable._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 143, 2)
    __propertytable._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 143, 2)
    
    propertytable = property(__propertytable.value, __propertytable.set, None, "The mapped dump property database table to be used for this item. Default is 'props'.")

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __table.name() : __table,
        __typecode.name() : __typecode,
        __propertytable.name() : __propertytable
    })
Namespace.addCategoryObject('typeBinding', 'deploymentType', deploymentType)


# Complex type relationType with content type ELEMENT_ONLY
class relationType (pyxb.binding.basis.complexTypeDefinition):
    """A RelationType defines a n-m or 1-n relation between types."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'relationType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 151, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element description uses Python identifier description
    __description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'description'), 'description', '__AbsentNamespace0_relationType_description', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 156, 2), )

    
    description = property(__description.value, __description.set, None, 'Provides possibility to add meaningfull description phrase for a generated model class.')

    
    # Element deployment uses Python identifier deployment
    __deployment = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'deployment'), 'deployment', '__AbsentNamespace0_relationType_deployment', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 161, 3), )

    
    deployment = property(__deployment.value, __deployment.set, None, 'Configures deployment information for this relation (table name and typecode).')

    
    # Element sourceElement uses Python identifier sourceElement
    __sourceElement = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'sourceElement'), 'sourceElement', '__AbsentNamespace0_relationType_sourceElement', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 166, 3), )

    
    sourceElement = property(__sourceElement.value, __sourceElement.set, None, 'Configures the generated attribute at source relation end')

    
    # Element targetElement uses Python identifier targetElement
    __targetElement = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'targetElement'), 'targetElement', '__AbsentNamespace0_relationType_targetElement', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 171, 3), )

    
    targetElement = property(__targetElement.value, __targetElement.set, None, 'Configures the generated attribute at target relation end')

    
    # Attribute code uses Python identifier code
    __code = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'code'), 'code', '__AbsentNamespace0_relationType_code', codeType, required=True)
    __code._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 177, 2)
    __code._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 177, 2)
    
    code = property(__code.value, __code.set, None, 'The typecode.')

    
    # Attribute localized uses Python identifier localized
    __localized = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'localized'), 'localized', '__AbsentNamespace0_relationType_localized', pyxb.binding.datatypes.boolean, required=True)
    __localized._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 182, 2)
    __localized._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 182, 2)
    
    localized = property(__localized.value, __localized.set, None, 'A localized n-m relation can have a link between two items for each language.')

    
    # Attribute deployment uses Python identifier deployment_
    __deployment_ = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'deployment'), 'deployment_', '__AbsentNamespace0_relationType_deployment_', deploymentRefType)
    __deployment_._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 187, 2)
    __deployment_._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 187, 2)
    
    deployment_ = property(__deployment_.value, __deployment_.set, None, "Deprecated, please use separate deployment sub tag. All instances of this type will be stored in a separated database table. The value of this attribute represents a reference to the specified deployment in the corresponding 'advanced-deployment.xml'. Default is empty.")

    
    # Attribute autocreate uses Python identifier autocreate
    __autocreate = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'autocreate'), 'autocreate', '__AbsentNamespace0_relationType_autocreate', pyxb.binding.datatypes.boolean, unicode_default='true')
    __autocreate._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 192, 2)
    __autocreate._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 192, 2)
    
    autocreate = property(__autocreate.value, __autocreate.set, None, "If 'true', the item will be created during initialization.")

    
    # Attribute generate uses Python identifier generate
    __generate = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'generate'), 'generate', '__AbsentNamespace0_relationType_generate', pyxb.binding.datatypes.boolean, unicode_default='true')
    __generate._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 197, 2)
    __generate._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 197, 2)
    
    generate = property(__generate.value, __generate.set, None, 'Deprecated. Will have no effect for relations.')

    _ElementMap.update({
        __description.name() : __description,
        __deployment.name() : __deployment,
        __sourceElement.name() : __sourceElement,
        __targetElement.name() : __targetElement
    })
    _AttributeMap.update({
        __code.name() : __code,
        __localized.name() : __localized,
        __deployment_.name() : __deployment_,
        __autocreate.name() : __autocreate,
        __generate.name() : __generate
    })
Namespace.addCategoryObject('typeBinding', 'relationType', relationType)


# Complex type relationElementType with content type ELEMENT_ONLY
class relationElementType (pyxb.binding.basis.complexTypeDefinition):
    """Configures the generated attribute at one relation end."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'relationElementType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 219, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element description uses Python identifier description
    __description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'description'), 'description', '__AbsentNamespace0_relationElementType_description', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 224, 3), )

    
    description = property(__description.value, __description.set, None, 'Documents this relation attribute. Will be cited at javadoc of generated getters/setters.')

    
    # Element modifiers uses Python identifier modifiers
    __modifiers = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'modifiers'), 'modifiers', '__AbsentNamespace0_relationElementType_modifiers', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 229, 3), )

    
    modifiers = property(__modifiers.value, __modifiers.set, None, 'Defines properties for the attribute.')

    
    # Element model uses Python identifier model
    __model = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'model'), 'model', '__AbsentNamespace0_relationElementType_model', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 234, 3), )

    
    model = property(__model.value, __model.set, None, 'Allows to configure model generation for this relation attribute used at servicelayer.')

    
    # Element custom-properties uses Python identifier custom_properties
    __custom_properties = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'custom-properties'), 'custom_properties', '__AbsentNamespace0_relationElementType_custom_properties', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 239, 3), )

    
    custom_properties = property(__custom_properties.value, __custom_properties.set, None, 'Allows to configure custom properties for the relation attribute.')

    
    # Attribute type uses Python identifier type
    __type = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'type'), 'type', '__AbsentNamespace0_relationElementType_type', codeType, required=True)
    __type._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 245, 2)
    __type._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 245, 2)
    
    type = property(__type.value, __type.set, None, 'Type of attribute which will be generated at type configured for opposite relation end.')

    
    # Attribute qualifier uses Python identifier qualifier
    __qualifier = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'qualifier'), 'qualifier', '__AbsentNamespace0_relationElementType_qualifier', pyxb.binding.datatypes.string)
    __qualifier._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 250, 2)
    __qualifier._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 250, 2)
    
    qualifier = property(__qualifier.value, __qualifier.set, None, 'Qualifier of attribute which will be generated at type configured for opposite relation end. If navigable is not set to false the qualifier is mandatory. Default is empty.')

    
    # Attribute metatype uses Python identifier metatype
    __metatype = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'metatype'), 'metatype', '__AbsentNamespace0_relationElementType_metatype', pyxb.binding.datatypes.string)
    __metatype._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 255, 2)
    __metatype._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 255, 2)
    
    metatype = property(__metatype.value, __metatype.set, None, "The (meta)type which describes the attributes type. Must be type extending RelationDescriptor. Default is 'RelationDescriptor'.")

    
    # Attribute cardinality uses Python identifier cardinality
    __cardinality = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'cardinality'), 'cardinality', '__AbsentNamespace0_relationElementType_cardinality', STD_ANON_)
    __cardinality._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 260, 2)
    __cardinality._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 260, 2)
    
    cardinality = property(__cardinality.value, __cardinality.set, None, "The cardinality of this relation end. Choose 'one' for 'one' part of a 1:n relation or 'many' when part of a n:m relation. A 1:1 relation is not supported. Default is 'many'.")

    
    # Attribute navigable uses Python identifier navigable
    __navigable = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'navigable'), 'navigable', '__AbsentNamespace0_relationElementType_navigable', pyxb.binding.datatypes.boolean)
    __navigable._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 279, 2)
    __navigable._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 279, 2)
    
    navigable = property(__navigable.value, __navigable.set, None, "Is the relation navigable from this side. Can only be disabled for one side of many to many relation. If disabled, no qualifier as well as modifiers can be defined. Default is 'true'.")

    
    # Attribute collectiontype uses Python identifier collectiontype
    __collectiontype = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'collectiontype'), 'collectiontype', '__AbsentNamespace0_relationElementType_collectiontype', STD_ANON_2)
    __collectiontype._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 284, 2)
    __collectiontype._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 284, 2)
    
    collectiontype = property(__collectiontype.value, __collectiontype.set, None, "Configures the type of this collection if element has cardinality 'many'. Related attribute getter / setter will use corresponding java collection interfaces. Default is 'Collection'.")

    
    # Attribute ordered uses Python identifier ordered
    __ordered = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'ordered'), 'ordered', '__AbsentNamespace0_relationElementType_ordered', pyxb.binding.datatypes.boolean)
    __ordered._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 308, 2)
    __ordered._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 308, 2)
    
    ordered = property(__ordered.value, __ordered.set, None, "If 'true' an additional ordering attribute will be generated for maintaining ordering. Default is 'false'.")

    _ElementMap.update({
        __description.name() : __description,
        __modifiers.name() : __modifiers,
        __model.name() : __model,
        __custom_properties.name() : __custom_properties
    })
    _AttributeMap.update({
        __type.name() : __type,
        __qualifier.name() : __qualifier,
        __metatype.name() : __metatype,
        __cardinality.name() : __cardinality,
        __navigable.name() : __navigable,
        __collectiontype.name() : __collectiontype,
        __ordered.name() : __ordered
    })
Namespace.addCategoryObject('typeBinding', 'relationElementType', relationElementType)


# Complex type enumtypeType with content type ELEMENT_ONLY
class enumtypeType (pyxb.binding.basis.complexTypeDefinition):
    """An EnumerationType defines fixed value types. (The typesystem provides item enumeration only)"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'enumtypeType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 315, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element description uses Python identifier description
    __description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'description'), 'description', '__AbsentNamespace0_enumtypeType_description', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 320, 3), )

    
    description = property(__description.value, __description.set, None, 'Provides possibility to add meaningfull description phrase for a generated model class.')

    
    # Element value uses Python identifier value_
    __value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'value'), 'value_', '__AbsentNamespace0_enumtypeType_value', True, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 325, 3), )

    
    value_ = property(__value.value, __value.set, None, 'Configures one value of this Enumeration.')

    
    # Attribute code uses Python identifier code
    __code = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'code'), 'code', '__AbsentNamespace0_enumtypeType_code', codeType, required=True)
    __code._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 331, 2)
    __code._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 331, 2)
    
    code = property(__code.value, __code.set, None, 'The unique code of this Enumeration.')

    
    # Attribute autocreate uses Python identifier autocreate
    __autocreate = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'autocreate'), 'autocreate', '__AbsentNamespace0_enumtypeType_autocreate', pyxb.binding.datatypes.boolean, unicode_default='true')
    __autocreate._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 336, 2)
    __autocreate._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 336, 2)
    
    autocreate = property(__autocreate.value, __autocreate.set, None, "If 'true', the item will be created during initialization.")

    
    # Attribute generate uses Python identifier generate
    __generate = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'generate'), 'generate', '__AbsentNamespace0_enumtypeType_generate', pyxb.binding.datatypes.boolean, unicode_default='true')
    __generate._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 341, 2)
    __generate._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 341, 2)
    
    generate = property(__generate.value, __generate.set, None, "If 'false' no constants will be generated at constant class of extension as well as at corresponding servicelayer enum class. Default is 'true'.")

    
    # Attribute jaloclass uses Python identifier jaloclass
    __jaloclass = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'jaloclass'), 'jaloclass', '__AbsentNamespace0_enumtypeType_jaloclass', classType)
    __jaloclass._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 346, 2)
    __jaloclass._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 346, 2)
    
    jaloclass = property(__jaloclass.value, __jaloclass.set, None, 'Specifies the name of the associated jalo class. The specified class must extend de.hybris.platform.jalo.enumeration.EnumerationValue and will not be generated. By specifying a jalo class you can change the implementation to use for the values of this enumeration. By default EnumerationValue class is used.')

    
    # Attribute dynamic uses Python identifier dynamic
    __dynamic = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'dynamic'), 'dynamic', '__AbsentNamespace0_enumtypeType_dynamic', pyxb.binding.datatypes.boolean)
    __dynamic._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 351, 2)
    __dynamic._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 351, 2)
    
    dynamic = property(__dynamic.value, __dynamic.set, None, "Whether it is possible to add new values by runtime. Also results in different types of enums: 'true' results in 'classic' hybris enums, 'false' results in Java enums. Default is false. Both kinds of enums are API compatible, and switching between enum types is possible by running a system update. ")

    _ElementMap.update({
        __description.name() : __description,
        __value.name() : __value
    })
    _AttributeMap.update({
        __code.name() : __code,
        __autocreate.name() : __autocreate,
        __generate.name() : __generate,
        __jaloclass.name() : __jaloclass,
        __dynamic.name() : __dynamic
    })
Namespace.addCategoryObject('typeBinding', 'enumtypeType', enumtypeType)


# Complex type itemtypeType with content type MIXED
class itemtypeType (pyxb.binding.basis.complexTypeDefinition):
    """Specifies a specific ComposedType."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_MIXED
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'itemtypeType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 635, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element description uses Python identifier description
    __description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'description'), 'description', '__AbsentNamespace0_itemtypeType_description', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 640, 3), )

    
    description = property(__description.value, __description.set, None, 'Provides possibility to add meaningfull description phrase for a generated model class.')

    
    # Element deployment uses Python identifier deployment
    __deployment = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'deployment'), 'deployment', '__AbsentNamespace0_itemtypeType_deployment', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 645, 3), )

    
    deployment = property(__deployment.value, __deployment.set, None, 'A deployment defines how a (generic) item or relation is mapped onto the database.')

    
    # Element custom-properties uses Python identifier custom_properties
    __custom_properties = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'custom-properties'), 'custom_properties', '__AbsentNamespace0_itemtypeType_custom_properties', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 650, 3), )

    
    custom_properties = property(__custom_properties.value, __custom_properties.set, None, 'Defines a list of custom properties for this type.')

    
    # Element attributes uses Python identifier attributes
    __attributes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'attributes'), 'attributes', '__AbsentNamespace0_itemtypeType_attributes', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 655, 3), )

    
    attributes = property(__attributes.value, __attributes.set, None, 'Defines the list of item attributes.')

    
    # Element indexes uses Python identifier indexes
    __indexes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'indexes'), 'indexes', '__AbsentNamespace0_itemtypeType_indexes', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 660, 3), )

    
    indexes = property(__indexes.value, __indexes.set, None, 'Defines the database indexes for this type.')

    
    # Element model uses Python identifier model
    __model = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'model'), 'model', '__AbsentNamespace0_itemtypeType_model', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 665, 3), )

    
    model = property(__model.value, __model.set, None, 'Allows to configure model generation for this item used at servicelayer.')

    
    # Attribute code uses Python identifier code
    __code = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'code'), 'code', '__AbsentNamespace0_itemtypeType_code', codeType, required=True)
    __code._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 671, 2)
    __code._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 671, 2)
    
    code = property(__code.value, __code.set, None, 'The unique code of this type.')

    
    # Attribute extends uses Python identifier extends
    __extends = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'extends'), 'extends', '__AbsentNamespace0_itemtypeType_extends', classType)
    __extends._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 676, 2)
    __extends._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 676, 2)
    
    extends = property(__extends.value, __extends.set, None, "Defines the class, which will be extended. Default is 'GenericItem'.")

    
    # Attribute jaloclass uses Python identifier jaloclass
    __jaloclass = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'jaloclass'), 'jaloclass', '__AbsentNamespace0_itemtypeType_jaloclass', classType)
    __jaloclass._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 681, 2)
    __jaloclass._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 681, 2)
    
    jaloclass = property(__jaloclass.value, __jaloclass.set, None, 'Specifies the name of the associated jalo class. Default is [extension-root-package].jalo.[type-code] which will be generated if not existent.')

    
    # Attribute deployment uses Python identifier deployment_
    __deployment_ = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'deployment'), 'deployment_', '__AbsentNamespace0_itemtypeType_deployment_', deploymentRefType)
    __deployment_._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 686, 2)
    __deployment_._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 686, 2)
    
    deployment_ = property(__deployment_.value, __deployment_.set, None, "Deprecated, please use separate deployment sub tag. All instances of this type will be stored in a separated database table. The value of this attribute represents a reference to the specified deployment in the corresponding 'advanced-deployment.xml'. Default is empty.")

    
    # Attribute singleton uses Python identifier singleton
    __singleton = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'singleton'), 'singleton', '__AbsentNamespace0_itemtypeType_singleton', pyxb.binding.datatypes.boolean)
    __singleton._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 691, 2)
    __singleton._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 691, 2)
    
    singleton = property(__singleton.value, __singleton.set, None, "If 'true', type gets marked as singleton which will be evaluated by some modules like hmc or impex, with that allowing only one instance per system. Default is 'false'.")

    
    # Attribute jaloonly uses Python identifier jaloonly
    __jaloonly = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'jaloonly'), 'jaloonly', '__AbsentNamespace0_itemtypeType_jaloonly', pyxb.binding.datatypes.boolean)
    __jaloonly._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 696, 2)
    __jaloonly._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 696, 2)
    
    jaloonly = property(__jaloonly.value, __jaloonly.set, None, "DEPRECATED. Use 'implements JaloOnlyItem' in your bean. If 'true', the item will only exists in the jalo layer and isn't backed by an entity bean. Default is 'false'.")

    
    # Attribute autocreate uses Python identifier autocreate
    __autocreate = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'autocreate'), 'autocreate', '__AbsentNamespace0_itemtypeType_autocreate', pyxb.binding.datatypes.boolean, unicode_default='true')
    __autocreate._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 701, 2)
    __autocreate._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 701, 2)
    
    autocreate = property(__autocreate.value, __autocreate.set, None, "If 'true', the item will be created during initialization. Default is 'true'.")

    
    # Attribute generate uses Python identifier generate
    __generate = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'generate'), 'generate', '__AbsentNamespace0_itemtypeType_generate', pyxb.binding.datatypes.boolean, unicode_default='true')
    __generate._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 706, 2)
    __generate._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 706, 2)
    
    generate = property(__generate.value, __generate.set, None, "If 'true', the sourcecode for this item will be created. Default is 'true'.")

    
    # Attribute abstract uses Python identifier abstract
    __abstract = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'abstract'), 'abstract', '__AbsentNamespace0_itemtypeType_abstract', pyxb.binding.datatypes.boolean)
    __abstract._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 711, 2)
    __abstract._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 711, 2)
    
    abstract = property(__abstract.value, __abstract.set, None, "Marks type and jalo class as abstract. If 'true', the type can not be instantiated. Default is 'false'.")

    
    # Attribute metatype uses Python identifier metatype
    __metatype = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'metatype'), 'metatype', '__AbsentNamespace0_itemtypeType_metatype', pyxb.binding.datatypes.string)
    __metatype._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 716, 2)
    __metatype._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 716, 2)
    
    metatype = property(__metatype.value, __metatype.set, None, "The (meta)type which describes the assigned type. Must be a type extensing ComposedType. Default is 'ComposedType'.")

    _ElementMap.update({
        __description.name() : __description,
        __deployment.name() : __deployment,
        __custom_properties.name() : __custom_properties,
        __attributes.name() : __attributes,
        __indexes.name() : __indexes,
        __model.name() : __model
    })
    _AttributeMap.update({
        __code.name() : __code,
        __extends.name() : __extends,
        __jaloclass.name() : __jaloclass,
        __deployment_.name() : __deployment_,
        __singleton.name() : __singleton,
        __jaloonly.name() : __jaloonly,
        __autocreate.name() : __autocreate,
        __generate.name() : __generate,
        __abstract.name() : __abstract,
        __metatype.name() : __metatype
    })
Namespace.addCategoryObject('typeBinding', 'itemtypeType', itemtypeType)


# Complex type maptypeType with content type EMPTY
class maptypeType (pyxb.binding.basis.complexTypeDefinition):
    """Like the java collection framework, a type, which defines map objects. Attention: When used as type for an attribute, the attribute will not be searchable and the access performance is not effective. Consider to use a relation."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'maptypeType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 796, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute code uses Python identifier code
    __code = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'code'), 'code', '__AbsentNamespace0_maptypeType_code', codeType, required=True)
    __code._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 800, 2)
    __code._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 800, 2)
    
    code = property(__code.value, __code.set, None, 'The unique code of the map.')

    
    # Attribute argumenttype uses Python identifier argumenttype
    __argumenttype = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'argumenttype'), 'argumenttype', '__AbsentNamespace0_maptypeType_argumenttype', classType, required=True)
    __argumenttype._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 805, 2)
    __argumenttype._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 805, 2)
    
    argumenttype = property(__argumenttype.value, __argumenttype.set, None, 'The type of the key attributes.')

    
    # Attribute returntype uses Python identifier returntype
    __returntype = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'returntype'), 'returntype', '__AbsentNamespace0_maptypeType_returntype', classType, required=True)
    __returntype._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 810, 2)
    __returntype._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 810, 2)
    
    returntype = property(__returntype.value, __returntype.set, None, 'The type of the value attributes.')

    
    # Attribute autocreate uses Python identifier autocreate
    __autocreate = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'autocreate'), 'autocreate', '__AbsentNamespace0_maptypeType_autocreate', pyxb.binding.datatypes.boolean, unicode_default='true')
    __autocreate._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 815, 2)
    __autocreate._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 815, 2)
    
    autocreate = property(__autocreate.value, __autocreate.set, None, "If 'true', the item will be created during initialization. Default is 'true'.")

    
    # Attribute generate uses Python identifier generate
    __generate = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'generate'), 'generate', '__AbsentNamespace0_maptypeType_generate', pyxb.binding.datatypes.boolean, unicode_default='true')
    __generate._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 820, 2)
    __generate._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 820, 2)
    
    generate = property(__generate.value, __generate.set, None, "Deprecated. Has no effect for map types. Default is 'true'.")

    
    # Attribute redeclare uses Python identifier redeclare
    __redeclare = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'redeclare'), 'redeclare', '__AbsentNamespace0_maptypeType_redeclare', pyxb.binding.datatypes.boolean)
    __redeclare._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 825, 2)
    __redeclare._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 825, 2)
    
    redeclare = property(__redeclare.value, __redeclare.set, None, "Deprecated. Has no effect for map types. Default is 'false'.")

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __code.name() : __code,
        __argumenttype.name() : __argumenttype,
        __returntype.name() : __returntype,
        __autocreate.name() : __autocreate,
        __generate.name() : __generate,
        __redeclare.name() : __redeclare
    })
Namespace.addCategoryObject('typeBinding', 'maptypeType', maptypeType)


# Complex type persistenceType with content type ELEMENT_ONLY
class persistenceType (pyxb.binding.basis.complexTypeDefinition):
    """Defines how the values of the attribute will be stored. Possible values: 'cmp' (deprecated), 'jalo' (not persistent), and 'property' (persistent)."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'persistenceType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 906, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element columntype uses Python identifier columntype
    __columntype = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'columntype'), 'columntype', '__AbsentNamespace0_persistenceType_columntype', True, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 911, 3), )

    
    columntype = property(__columntype.value, __columntype.set, None, 'Configures a persistence definition for a specific database used at create statement.')

    
    # Attribute type uses Python identifier type
    __type = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'type'), 'type', '__AbsentNamespace0_persistenceType_type', STD_ANON_3, required=True)
    __type._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 917, 2)
    __type._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 917, 2)
    
    type = property(__type.value, __type.set, None, "Defines how the values of the attribute will be stored. Possible values: 'cmp' (deprecated), 'jalo' (not persistent, deprecated), 'property' (persistent), 'dynamic' (not persisted).")

    
    # Attribute qualifier uses Python identifier qualifier
    __qualifier = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'qualifier'), 'qualifier', '__AbsentNamespace0_persistenceType_qualifier', pyxb.binding.datatypes.string)
    __qualifier._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 946, 2)
    __qualifier._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 946, 2)
    
    qualifier = property(__qualifier.value, __qualifier.set, None, "Deprecated. Only usable in relation with 'cmp' and 'property'(compatibility reasons) persistence type. Default is empty.")

    
    # Attribute attributeHandler uses Python identifier attributeHandler
    __attributeHandler = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'attributeHandler'), 'attributeHandler', '__AbsentNamespace0_persistenceType_attributeHandler', pyxb.binding.datatypes.string)
    __attributeHandler._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 951, 2)
    __attributeHandler._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 951, 2)
    
    attributeHandler = property(__attributeHandler.value, __attributeHandler.set, None, 'Spring bean id that handles dynamic attributes implementation.')

    _ElementMap.update({
        __columntype.name() : __columntype
    })
    _AttributeMap.update({
        __type.name() : __type,
        __qualifier.name() : __qualifier,
        __attributeHandler.name() : __attributeHandler
    })
Namespace.addCategoryObject('typeBinding', 'persistenceType', persistenceType)


# Complex type valueType with content type EMPTY
class valueType (pyxb.binding.basis.complexTypeDefinition):
    """Configures a single element."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'valueType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 986, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute code uses Python identifier code
    __code = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'code'), 'code', '__AbsentNamespace0_valueType_code', codeType, required=True)
    __code._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 990, 2)
    __code._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 990, 2)
    
    code = property(__code.value, __code.set, None, 'The unique code of this element.')

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __code.name() : __code
    })
Namespace.addCategoryObject('typeBinding', 'valueType', valueType)


# Complex type enumValueType with content type ELEMENT_ONLY
class enumValueType (pyxb.binding.basis.complexTypeDefinition):
    """Configures a single enum value."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'enumValueType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 997, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element description uses Python identifier description
    __description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'description'), 'description', '__AbsentNamespace0_enumValueType_description', False, pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 1002, 3), )

    
    description = property(__description.value, __description.set, None, 'Provides possibility to add meaningfull description phrase for a generated model class.')

    
    # Attribute code uses Python identifier code
    __code = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'code'), 'code', '__AbsentNamespace0_enumValueType_code', enumCodeType, required=True)
    __code._DeclarationLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 1008, 2)
    __code._UseLocation = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 1008, 2)
    
    code = property(__code.value, __code.set, None, 'The unique code of this element.')

    _ElementMap.update({
        __description.name() : __description
    })
    _AttributeMap.update({
        __code.name() : __code
    })
Namespace.addCategoryObject('typeBinding', 'enumValueType', enumValueType)


items = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'items'), CTD_ANON, documentation='Defines the types of your extension.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 756, 1))
Namespace.addCategoryObject('elementBinding', items.name().localName(), items)



atomictypesType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'atomictype'), atomictypeType, scope=atomictypesType, documentation="An AtomicType represents a simple java object. (The name 'atomic' just means 'non-composed' objects.)", location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 57, 3)))

def _BuildAutomaton ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton
    del _BuildAutomaton
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 57, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(atomictypesType._UseForTag(pyxb.namespace.ExpandedName(None, 'atomictype')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 57, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
atomictypesType._Automaton = _BuildAutomaton()




collectiontypesType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'collectiontype'), collectiontypeType, scope=collectiontypesType, documentation='A CollectionType defines a collection of typed elements.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 121, 3)))

def _BuildAutomaton_ ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_
    del _BuildAutomaton_
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 121, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(collectiontypesType._UseForTag(pyxb.namespace.ExpandedName(None, 'collectiontype')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 121, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
collectiontypesType._Automaton = _BuildAutomaton_()




relationsType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'relation'), relationType, scope=relationsType, documentation='A RelationType defines a n-m or 1-n relation between types.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 211, 3)))

def _BuildAutomaton_2 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_2
    del _BuildAutomaton_2
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 211, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(relationsType._UseForTag(pyxb.namespace.ExpandedName(None, 'relation')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 211, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
relationsType._Automaton = _BuildAutomaton_2()




enumtypesType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'enumtype'), enumtypeType, scope=enumtypesType, documentation='An EnumerationType defines fixed value types. (The typesystem provides item enumeration only)', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 364, 3)))

def _BuildAutomaton_3 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_3
    del _BuildAutomaton_3
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 364, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(enumtypesType._UseForTag(pyxb.namespace.ExpandedName(None, 'enumtype')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 364, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
enumtypesType._Automaton = _BuildAutomaton_3()




indexType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'key'), indexKeyType, scope=indexType, documentation='Configures a single index key.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 377, 3)))

def _BuildAutomaton_4 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_4
    del _BuildAutomaton_4
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 377, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(indexType._UseForTag(pyxb.namespace.ExpandedName(None, 'key')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 377, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
indexType._Automaton = _BuildAutomaton_4()




attributeType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'defaultvalue'), defaultValueType, scope=attributeType, documentation='Configures a default value for this attribute used if no value is provided. The default value is calculated by initialization and will not be re-calculated by runtime.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 426, 3)))

attributeType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'description'), pyxb.binding.datatypes.string, scope=attributeType, documentation='Gives a description for this attribute only used for the javadoc of generated attribute methods.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 431, 3)))

attributeType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'persistence'), persistenceType, scope=attributeType, documentation="Defines how the values of the attribute will be stored. Possible values: 'cmp' (deprecated), 'jalo' (not persistent, deprecated), 'property' (persistent), 'dynamic' (not persisted).", location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 436, 3)))

attributeType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'modifiers'), modifiersType, scope=attributeType, documentation='Configures advanced settings for this attribute definition.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 441, 3)))

attributeType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'custom-properties'), customPropertiesType, scope=attributeType, documentation='Allows to configure custom properties for this attribute.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 446, 3)))

attributeType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'model'), attributeModelType, scope=attributeType, documentation='Allows to configure model generation settings for this attribute. Models are used by the hybris ServiceLayer.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 451, 3)))

def _BuildAutomaton_6 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_6
    del _BuildAutomaton_6
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 426, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(attributeType._UseForTag(pyxb.namespace.ExpandedName(None, 'defaultvalue')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 426, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=st_0)

def _BuildAutomaton_7 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_7
    del _BuildAutomaton_7
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 431, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(attributeType._UseForTag(pyxb.namespace.ExpandedName(None, 'description')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 431, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=st_0)

def _BuildAutomaton_8 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_8
    del _BuildAutomaton_8
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 436, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(attributeType._UseForTag(pyxb.namespace.ExpandedName(None, 'persistence')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 436, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=st_0)

def _BuildAutomaton_9 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_9
    del _BuildAutomaton_9
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 441, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(attributeType._UseForTag(pyxb.namespace.ExpandedName(None, 'modifiers')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 441, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=st_0)

def _BuildAutomaton_10 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_10
    del _BuildAutomaton_10
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 446, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(attributeType._UseForTag(pyxb.namespace.ExpandedName(None, 'custom-properties')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 446, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=st_0)

def _BuildAutomaton_11 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_11
    del _BuildAutomaton_11
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 451, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(attributeType._UseForTag(pyxb.namespace.ExpandedName(None, 'model')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 451, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=st_0)

def _BuildAutomaton_5 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_5
    del _BuildAutomaton_5
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 426, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 431, 3))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 436, 3))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 441, 3))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 446, 3))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 451, 3))
    counters.add(cc_5)
    states = []
    sub_automata = []
    sub_automata.append(_BuildAutomaton_6())
    sub_automata.append(_BuildAutomaton_7())
    sub_automata.append(_BuildAutomaton_8())
    sub_automata.append(_BuildAutomaton_9())
    sub_automata.append(_BuildAutomaton_10())
    sub_automata.append(_BuildAutomaton_11())
    final_update = set()
    symbol = pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 425, 2)
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=True)
    st_0._set_subAutomata(*sub_automata)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
attributeType._Automaton = _BuildAutomaton_5()




attributeModelType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'getter'), modelMethodType, scope=attributeModelType, documentation='\n\t\t\t\t\t\tAllows to configure alternative getter methods at generated model.\n\t\t\t\t\t', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 499, 3)))

attributeModelType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'setter'), modelMethodType, scope=attributeModelType, documentation='\n\t\t\t\t\t\tAllows to configure alternative setter methods at generated model.\n\t\t\t\t\t', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 506, 3)))

def _BuildAutomaton_12 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_12
    del _BuildAutomaton_12
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 499, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 506, 3))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(attributeModelType._UseForTag(pyxb.namespace.ExpandedName(None, 'getter')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 499, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(attributeModelType._UseForTag(pyxb.namespace.ExpandedName(None, 'setter')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 506, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
attributeModelType._Automaton = _BuildAutomaton_12()




itemModelType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'constructor'), modelConstructorType, scope=itemModelType, documentation='\n\t\t\t\t\t\tAllows to configure model constructor signatures.\n\t\t\t\t\t', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 526, 3)))

def _BuildAutomaton_13 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_13
    del _BuildAutomaton_13
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 526, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(itemModelType._UseForTag(pyxb.namespace.ExpandedName(None, 'constructor')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 526, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
itemModelType._Automaton = _BuildAutomaton_13()




customPropertiesType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'property'), customPropertyType, scope=customPropertiesType, documentation='Defines a custom property.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 583, 3)))

def _BuildAutomaton_14 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_14
    del _BuildAutomaton_14
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 583, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(customPropertiesType._UseForTag(pyxb.namespace.ExpandedName(None, 'property')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 583, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
customPropertiesType._Automaton = _BuildAutomaton_14()




customPropertyType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'value'), defaultValueType, scope=customPropertyType, documentation='The value of the custom property.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 596, 3)))

def _BuildAutomaton_15 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_15
    del _BuildAutomaton_15
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(customPropertyType._UseForTag(pyxb.namespace.ExpandedName(None, 'value')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 596, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
customPropertyType._Automaton = _BuildAutomaton_15()




attributesType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'attribute'), attributeType, scope=attributesType, documentation='Defines a single attribute.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 614, 3)))

def _BuildAutomaton_16 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_16
    del _BuildAutomaton_16
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 614, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(attributesType._UseForTag(pyxb.namespace.ExpandedName(None, 'attribute')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 614, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
attributesType._Automaton = _BuildAutomaton_16()




indexesType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'index'), indexType, scope=indexesType, documentation='Configures a single index.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 627, 3)))

def _BuildAutomaton_17 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_17
    del _BuildAutomaton_17
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(indexesType._UseForTag(pyxb.namespace.ExpandedName(None, 'index')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 627, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
indexesType._Automaton = _BuildAutomaton_17()




itemtypesType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'itemtype'), itemtypeType, scope=itemtypesType, documentation='Specifies a specific ComposedType.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 728, 3)))

itemtypesType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'typegroup'), typeGroupType, scope=itemtypesType, documentation='Specifies a group of ComposedTypes to allow better structuring within the items.xml file.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 733, 3)))

def _BuildAutomaton_18 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_18
    del _BuildAutomaton_18
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 728, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 733, 3))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(itemtypesType._UseForTag(pyxb.namespace.ExpandedName(None, 'itemtype')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 728, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(itemtypesType._UseForTag(pyxb.namespace.ExpandedName(None, 'typegroup')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 733, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
itemtypesType._Automaton = _BuildAutomaton_18()




typeGroupType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'itemtype'), itemtypeType, scope=typeGroupType, documentation='Specifies a specific ComposedType.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 743, 3)))

def _BuildAutomaton_19 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_19
    del _BuildAutomaton_19
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 743, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(typeGroupType._UseForTag(pyxb.namespace.ExpandedName(None, 'itemtype')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 743, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
typeGroupType._Automaton = _BuildAutomaton_19()




CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'atomictypes'), atomictypesType, scope=CTD_ANON, documentation="Defines the list of AtomicType's for your extension.", location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 762, 4)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'collectiontypes'), collectiontypesType, scope=CTD_ANON, documentation="Defines the list of CollectionType's for your extension.", location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 767, 4)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'enumtypes'), enumtypesType, scope=CTD_ANON, documentation="Defines the list of EnumerationType's for your extension.", location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 772, 4)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'maptypes'), maptypesType, scope=CTD_ANON, documentation="Defines the list of MapType's for your extension.", location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 777, 4)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'relations'), relationsType, scope=CTD_ANON, documentation="Defines the list of RelationType's for your extension.", location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 782, 4)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'itemtypes'), itemtypesType, scope=CTD_ANON, documentation="Defines the list of ComposedType's for your extension.", location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 787, 4)))

def _BuildAutomaton_20 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_20
    del _BuildAutomaton_20
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 762, 4))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 767, 4))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 772, 4))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 777, 4))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 782, 4))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 787, 4))
    counters.add(cc_5)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(None, 'atomictypes')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 762, 4))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(None, 'collectiontypes')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 767, 4))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(None, 'enumtypes')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 772, 4))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(None, 'maptypes')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 777, 4))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(None, 'relations')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 782, 4))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(None, 'itemtypes')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 787, 4))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    st_5._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
CTD_ANON._Automaton = _BuildAutomaton_20()




maptypesType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'maptype'), maptypeType, scope=maptypesType, documentation='Like the java collection framework, a type, which defines map objects. Attention: When used as type for an attribute, the attribute will not be searchable and the access performance is not effective. Consider to use a relation.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 837, 3)))

def _BuildAutomaton_21 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_21
    del _BuildAutomaton_21
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 837, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(maptypesType._UseForTag(pyxb.namespace.ExpandedName(None, 'maptype')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 837, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
maptypesType._Automaton = _BuildAutomaton_21()




columntypeType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'value'), STD_ANON_4, scope=columntypeType, documentation="The attribute type used in the create statement of the database table, such as 'varchar2(4000)'.", location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 963, 3)))

def _BuildAutomaton_22 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_22
    del _BuildAutomaton_22
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(columntypeType._UseForTag(pyxb.namespace.ExpandedName(None, 'value')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 963, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
columntypeType._Automaton = _BuildAutomaton_22()




relationType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'description'), pyxb.binding.datatypes.string, scope=relationType, documentation='Provides possibility to add meaningfull description phrase for a generated model class.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 156, 2)))

relationType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'deployment'), deploymentType, scope=relationType, documentation='Configures deployment information for this relation (table name and typecode).', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 161, 3)))

relationType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'sourceElement'), relationElementType, scope=relationType, documentation='Configures the generated attribute at source relation end', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 166, 3)))

relationType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'targetElement'), relationElementType, scope=relationType, documentation='Configures the generated attribute at target relation end', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 171, 3)))

def _BuildAutomaton_23 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_23
    del _BuildAutomaton_23
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 156, 2))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 161, 3))
    counters.add(cc_1)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(relationType._UseForTag(pyxb.namespace.ExpandedName(None, 'description')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 156, 2))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(relationType._UseForTag(pyxb.namespace.ExpandedName(None, 'deployment')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 161, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(relationType._UseForTag(pyxb.namespace.ExpandedName(None, 'sourceElement')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 166, 3))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(relationType._UseForTag(pyxb.namespace.ExpandedName(None, 'targetElement')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 171, 3))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
relationType._Automaton = _BuildAutomaton_23()




relationElementType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'description'), pyxb.binding.datatypes.string, scope=relationElementType, documentation='Documents this relation attribute. Will be cited at javadoc of generated getters/setters.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 224, 3)))

relationElementType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'modifiers'), modifiersType, scope=relationElementType, documentation='Defines properties for the attribute.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 229, 3)))

relationElementType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'model'), attributeModelType, scope=relationElementType, documentation='Allows to configure model generation for this relation attribute used at servicelayer.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 234, 3)))

relationElementType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'custom-properties'), customPropertiesType, scope=relationElementType, documentation='Allows to configure custom properties for the relation attribute.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 239, 3)))

def _BuildAutomaton_24 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_24
    del _BuildAutomaton_24
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 224, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 229, 3))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 234, 3))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 239, 3))
    counters.add(cc_3)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(relationElementType._UseForTag(pyxb.namespace.ExpandedName(None, 'description')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 224, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(relationElementType._UseForTag(pyxb.namespace.ExpandedName(None, 'modifiers')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 229, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(relationElementType._UseForTag(pyxb.namespace.ExpandedName(None, 'model')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 234, 3))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(relationElementType._UseForTag(pyxb.namespace.ExpandedName(None, 'custom-properties')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 239, 3))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
relationElementType._Automaton = _BuildAutomaton_24()




enumtypeType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'description'), pyxb.binding.datatypes.string, scope=enumtypeType, documentation='Provides possibility to add meaningfull description phrase for a generated model class.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 320, 3)))

enumtypeType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'value'), enumValueType, scope=enumtypeType, documentation='Configures one value of this Enumeration.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 325, 3)))

def _BuildAutomaton_25 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_25
    del _BuildAutomaton_25
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 320, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 325, 3))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(enumtypeType._UseForTag(pyxb.namespace.ExpandedName(None, 'description')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 320, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(enumtypeType._UseForTag(pyxb.namespace.ExpandedName(None, 'value')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 325, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
enumtypeType._Automaton = _BuildAutomaton_25()




itemtypeType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'description'), pyxb.binding.datatypes.string, scope=itemtypeType, documentation='Provides possibility to add meaningfull description phrase for a generated model class.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 640, 3)))

itemtypeType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'deployment'), deploymentType, scope=itemtypeType, documentation='A deployment defines how a (generic) item or relation is mapped onto the database.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 645, 3)))

itemtypeType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'custom-properties'), customPropertiesType, scope=itemtypeType, documentation='Defines a list of custom properties for this type.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 650, 3)))

itemtypeType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'attributes'), attributesType, scope=itemtypeType, documentation='Defines the list of item attributes.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 655, 3)))

itemtypeType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'indexes'), indexesType, scope=itemtypeType, documentation='Defines the database indexes for this type.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 660, 3)))

itemtypeType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'model'), itemModelType, scope=itemtypeType, documentation='Allows to configure model generation for this item used at servicelayer.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 665, 3)))

def _BuildAutomaton_26 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_26
    del _BuildAutomaton_26
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 640, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 645, 3))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 650, 3))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 655, 3))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 660, 3))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 665, 3))
    counters.add(cc_5)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(itemtypeType._UseForTag(pyxb.namespace.ExpandedName(None, 'description')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 640, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(itemtypeType._UseForTag(pyxb.namespace.ExpandedName(None, 'deployment')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 645, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(itemtypeType._UseForTag(pyxb.namespace.ExpandedName(None, 'custom-properties')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 650, 3))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(itemtypeType._UseForTag(pyxb.namespace.ExpandedName(None, 'attributes')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 655, 3))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(itemtypeType._UseForTag(pyxb.namespace.ExpandedName(None, 'indexes')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 660, 3))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(itemtypeType._UseForTag(pyxb.namespace.ExpandedName(None, 'model')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 665, 3))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    st_5._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
itemtypeType._Automaton = _BuildAutomaton_26()




persistenceType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'columntype'), columntypeType, scope=persistenceType, documentation='Configures a persistence definition for a specific database used at create statement.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 911, 3)))

def _BuildAutomaton_27 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_27
    del _BuildAutomaton_27
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 911, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(persistenceType._UseForTag(pyxb.namespace.ExpandedName(None, 'columntype')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 911, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
persistenceType._Automaton = _BuildAutomaton_27()




enumValueType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'description'), pyxb.binding.datatypes.string, scope=enumValueType, documentation='Provides possibility to add meaningfull description phrase for a generated model class.', location=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 1002, 3)))

def _BuildAutomaton_28 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_28
    del _BuildAutomaton_28
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 1002, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(enumValueType._UseForTag(pyxb.namespace.ExpandedName(None, 'description')), pyxb.utils.utility.Location('/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd', 1002, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
enumValueType._Automaton = _BuildAutomaton_28()

