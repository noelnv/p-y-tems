__author__ = 'nnoel'

# Put your configuration parameters here.

HYBRIS_CUSTOM_ROOT = '/Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom'

ITEMS_XML_PATHS = ['/tpc/tpccore/resources/tpccore-items.xml',
                   '/advancedexport/resources/advancedexport-items.xml',
                   '/incentivescockpit/resources/incentivescockpit-items.xml',
                   '/recipecockpit/resources/recipecockpit-items.xml',
                   '/stockimagecockpit/resources/stockimagecockpit-items.xml',
                   '/tpc/tpccore/resources/tpccore-items/tpccore-cms-items.xml',
                   '/tpc/tpccore/resources/tpccore-items/tpccore-common-items.xml',
                   '/tpc/tpccore/resources/tpccore-items/tpccore-orders-items.xml',
                   '/tpc/tpccore/resources/tpccore-items/tpccore-products-items.xml',
                   '/tpc/tpccore/resources/tpccore-items/tpccore-promotions-items.xml',
                   '/tpc/tpccore/resources/tpccore-items/tpccore-recipes-items.xml',
                   '/tpc/tpccore/resources/tpccore-items/tpccore-users-items.xml',
                   '/tpc/tpcrestservices/resources/tpcrestservices-items.xml',
                   '/tpc/tpcwebservices/resources/tpcwebservices-items.xml'
                   ]
HYBRIS_REPORT_XLSX_FILE = '/Users/nnoel/Downloads/pampered-chef-tsv-08062015.xlsx'
