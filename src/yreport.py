__author__ = 'nnoel'

from openpyxl import load_workbook

COL_FILENAME = (0, 'fileName')
COL_LINE_NUMBER = (1, 'lineNumber')
COL_RULE = (2, 'rule')
COL_PRIORITY = (3, 'priority')
COL_STATE = (4, 'state')
COL_ELEMENT = (5, 'element')
COL_DESC = (6, 'description')


class YReport:
    def __init__(self, report_filename, target_filename):
        self.xls_filter = {
            COL_FILENAME[0]: [target_filename],
            COL_PRIORITY[0]: ['H'],
            COL_STATE[0]: ['FAIL']
        }

        wb = load_workbook(report_filename)
        sheet = wb.worksheets[0]
        results = list()

        for row in sheet.rows:
            if YReport.row_passes_filter(row, self.xls_filter):
                d = YReport.extract_row_data(row)
                results.append(d)

        self.rows = results
        self.types = set()
        for row in self.rows:
            self.types.add(row['element'])

    @staticmethod
    def row_passes_filter(row, row_filter):
        passes = True
        for k, v in row_filter.iteritems():
            passes &= row[k].value in v
            if not passes:
                return passes

        return passes

    @staticmethod
    def extract_row_data(row):
        return {
            COL_FILENAME[1]: row[COL_FILENAME[0]].value,
            COL_LINE_NUMBER[1]: row[COL_LINE_NUMBER[0]].value,
            COL_RULE[1]: row[COL_RULE[0]].value,
            COL_PRIORITY[1]: row[COL_PRIORITY[0]].value,
            COL_STATE[1]: row[COL_STATE[0]].value,
            COL_ELEMENT[1]: row[COL_ELEMENT[0]].value,
            COL_DESC[1]: row[COL_DESC[0]].value,
        }
