import yitems
from yreport import YReport
from ygraph import YGraph
import config


if __name__ == '__main__':

    for file_path in config.ITEMS_XML_PATHS:
        file_path = config.HYBRIS_CUSTOM_ROOT+file_path
        file_name = file_path.split('/')[-1]
        print 'processing', file_name

        xml = open(file_path).read()
        y_items = yitems.CreateFromDocument(xml)
        y_report = YReport(config.HYBRIS_REPORT_XLSX_FILE, file_name)
        y_graph = YGraph(y_items, y_report)
        y_graph.draw('../output/'+file_name+'_graph.png')
        y_graph.report('../output/'+file_name+'_report.xlsx')


