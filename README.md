# README #

This README would normally document whatever steps are necessary to get your application up and running.  Wouldn't it be nice if it did that?

### What is this repository for? ###

* This is a tool that generates a graphical representation of a hybris items.xml file. This is useful when a bunch of other people designed your data model and you don't know what is in it.  Additionally, it combines data with a report file that some hybris consultants gave us with some items.xml rule violations.  The resulting graph will have both red and green nodes.
* Current version is 0.nothing

### How do I get set up? ###
ENVIRONMENT
1. You need normal python tools: virtualenv, pip etc.  I won't teach you how to use these things here. I assume you know how to use them.

2. run: source env/bin/activate to get into the virtual env.

3. install the requirements: pip install -r requirements.txt

4. This project uses pyxb to generate a binding to the items xml schema.  This project should already have a binding
but if you need to regenerate it:
cd src
pyxbgen -u <path to items.xsd> -m yitems

e.g.
pyxbgen -u /Users/nnoel/tpc/ecommerce-hybris-stable/bin/custom/tpc/tpccore/resources/items.xsd -m yitems

CONFIGURATION
In /src there is config.py it holds some constants representing paths to files.  
HYBRIS_CUSTOM_ROOT: The absolute path to the 'custom' folder of your hybris project.
ITEMS_XML_PATHS: The relative paths from HYBRIS_CUSTOM_ROOT of each items.xml file you want to process.

HYBRIS_REPORT_XLSX_FILE: The absolute path to the report file from the hybris people.  Ideally this would be optional, but it's totally required. 


### Contribution guidelines ###
Some ideas for features would be:
* Make hybris report optional.
* Have differing degrees of rule violations i.e green, yellow, orange, red nodes.
* Have a way of querying for the number of items in the database, to get a sense of how much data is stored in each type.
* Have a way of connecting the different diagrams together with HTML pages.  Have a way of parameterizing the size of the graphics.
* Output to dot file so graphviz could be run / the diagram could be version controlled without having to be regenerated from scratch.

### Who do I talk to? ###
If you have questions you can talk to https://bitbucket.org/noelnv. I'll try to answer any questions within 6-8 weeks.